# gx-idris

gx-idris is a [gx](https://github.com/whyrusleeping/gx) hook handler
for [Idris](https://idris-lang.org/).
It allows you to use gx as a package manager for Idris,
on top of the package functionality already built in. MIT licensed.

gx-idris requires gx itself and currently also GNU grep
(on top of regular POSIX utilities).

## Installation

Copy or link `gx-idris` somewhere in your `$PATH`.

## Operation

Below is a walk-through of converting an existing Idris project
into a gx package and adding another Idris gx package as a dependency:

-   `gx init --lang idris`

    Initialises the current directory as an Idris gx package.

-   `gx-idris ipkg set my-package.ipkg`

    Tell gx-idris that this is the operating Idris package file.
    It does not have to exist, but a warning will be issued if that is the case.

    If the ipkg has an `opts` field, it will be recorded in package.json.
    This is because gx-idris will need to write to this field when dependencies
    are added.

-   `gx import [some package hash]`
    
    If the import is an Idris package, the operating ipkg file
    will be updated so that the `opts` field includes `-i package-dir`.

-   `gx-idris package build`

    Compile the current package to check everything works.

-   `gx-idris --`

    Start an Idris shell with Idris gx dependencies in the include search path.

## Commands

```
gx-idris ipkg set FILE
```
Set FILE as the current ipkg for this gx package.

---
```
gx-idris ipkg reset-opts
```
Reset the `opts` field in the current ipkg as described in
the Operation section.

You shouldn't need to use this unless you manually edit the
`opts` field of the ipkg or the `.idris.opts` field in package.json.

---
```
gx-idris package build
```
Build the current ipkg with `idris --build`.

---
```
gx-idris -- ARGS ...
```
Invoke `idris` with:
- the arguments listed in `.idris.opts` in package.json,
- include flags for each Idris package dependency,
- `ARGS ...` as given
as arguments, in that order.

## To do

- Allow a configurable include path per package
- Allow multiple ipkg files
- Test in various configurations
- Add more operations to `gx-idris package`

## Contribution
Please do!
